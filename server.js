//Set up variable, required modules
var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var session = require('express-session');
var parseurl = require('parseurl');
var cookieParser = require('cookie-parser');
var auth = require('./api/auth/auth.service');
var mongoose = require('mongoose');
//==========================================
//Set the port for API
var port = process.env.PORT || 7000;
//==========================================

//use bodyParser to get data from post request
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(cookieParser('studentHero', {maxAge: 120}));
// app.use(session({
//   secret: "studentHero"
// }));

//log request to the console
app.use(morgan('dev')); //color output

//handle CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
//==========================================


//mongoose setup database where to save
mongoose.connect('mongodb://admin:adminpass@ds155097.mlab.com:55097/studentsche');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'DB connection error: '));
db.once('open', function() {
  console.log('DB connection success! ');
});//check if connect success to Dbmongo
//==========================================


//====Routes====
app.use('/schedule', auth.hasRole('student'), express.static(__dirname + "/client/schedule"));
//app.use('/admin', auth.hasRole('admin'), express.static(__dirname + "/client/admin"));
app.use('/login', express.static(__dirname + "/client/login"));
app.use('/', express.static(__dirname + "/client"));
require('./route')(app);
//====Start server====
app.listen(port);
console.log(port + " is connected"); 

