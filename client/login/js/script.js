$('input[type="text"],input[type="password"]').focus(function() {
  $(this).prev().animate({
    'opacity': '1'
  }, 200)
});
$('input[type="text"],input[type="password"]').blur(function() {
  $(this).prev().animate({
    'opacity': '.5'
  }, 200)
});

$('input[type="text"],input[type="password"]').keyup(function() {
  if (!$(this).val() == '') {
    $(this).next().animate({
      'opacity': '1',
      'right': '3%'
    }, 200)
  } else {
    $(this).next().animate({
      'opacity': '0',
      'right': '2%'
    }, 200)
  }
});

$('body').on('click', 'button', function(){
  var username = $('input[type="text"]').val();
  var password = $('input[type="password"]').val();
  $.ajax({
    url: "/api/auth/local",//POST data login to server for authentication
    method: "POST",
    contentType : "application/json",
    data: JSON.stringify({"username": username, "password": password})
  }).done(function(response) {
  if (response.token) {
      console.log(response);
      document.cookie = "token=" + response.token;
      if(response.referrer){
        console.log(response.referrer);
        window.location.href = "../../schedule";
      }else{
        console.log(response);
      };
    };
  });
})