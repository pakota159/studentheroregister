moment().format();

var countSelected = function(){
	var count = 0;
	$('table td').each(function(){
		if($(this).hasClass('selected') == true){
			count += 1;
		}
	});
	return count;
}

var toggleCl = function(){
	$('table').on('click', 'td:not(.date):not(.empty):not(.time):not(.noneSlot)', function(){
		$(this).toggleClass( "selected" );
		$('td').not(this).removeClass("selected");
		var count = countSelected();
		if(count == 0){
			$('td').css('opacity', '1');
		}else{
			$('td:not(.date):not(.empty):not(.time):not(.selected)').css('opacity', '0.5');
			$('td.selected').css('opacity', '1');
		}
	});
}

var compare = function(day, time, slots){
	var count = 0;
	for(var i = 0; i< slots.length; i++){
		var slotTime = slots[i].scheHour.toString()+slots[i].scheMinute.toString();
		if(slotTime === time && day === slots[i].scheDay){
			return [slots[i].remainSlots, slots[i].scheTimeStamp/1000];
		}
	};
	return 0;
};

var addHeaderRow = function(days){
	var element = '';					    
	element += "<tr id='headRow'>";
	element += "<td class='empty'></td>";
	days.forEach(function(val){
		element += "<td class='date'>"+val+"</td>";	
	})
	element += "</tr>";
	$('table tbody').append(element);
}

var addSlots = function(slots, days, times){
	var element = '';
	times.forEach(function(time){
		var id = time.split(" ").join("").replace(':', '');
		element += "<tr id='"+id+"'>";
		element += "<td class='time'>"+time+"</td>";
		days.forEach(function(day){
			if(compare(day, id, slots) === 0 || compare(day, id, slots)[0] === 0){
				element += "<td class='noneSlot'><span>"+0+"</span> slots remain</td>";
			}else{
				element += "<td data-id="+compare(day, id, slots)[1]+"><span>"+compare(day, id, slots)[0]+"</span> slots remain</td>";
			}
		});
		element += "</tr>";
	});
	$('table tbody').append(element);
						    
}

var submit = function(response){
	
	$('body').on('click', 'button', function(){
		var count = countSelected();
		if (count == 0){
			alert("Xin hãy chọn một ca phỏng vấn");
		}else{
			if(confirm("Bạn có chắc chắn chọn ca phỏng vấn này không?")){
				var timeStamp = '';
				$('table td').each(function(){
					if($(this).hasClass('selected') == true){
						timeStamp = $(this).attr('data-id');
					}
				});
				$.ajax({
					url: "/api/user",
					method: "PUT",
					data: {timeStamp : timeStamp}
				}).done(function(response){
					console.log(response);
						$.ajax({
						url: "/api/sche",
						method: "PUT",
						data: {timeStamp : timeStamp}
					}).done(function(res){
						console.log(res);
						location.reload(true);
					});
				});
			};
		}
	});
}

var main = function(){
	$(document).ready(function(){
		$.ajax({
			url: "/api/sche",
			method: "GET",
			contentType: "application/json",
		}).done(function(response){
			if(response.length === 1){
				console.log(response[0])
				var dateUtc = moment.unix(response[0]);
				var day = moment(dateUtc).local();
				var hour = day.hour().toString();
				var minute = day.minute().toString();
				var weekDay = moment(response[0]*1000).format("dddd, MMMM Do YYYY");
				console.log(weekDay);
				if(hour.length === 1){
						hour = '0' + hour;
				};
				if(minute.length === 1){
					minute = '0' + minute;
				};
				var time = hour+" : "+minute
				$.ajax({
				    url: "/api/sendEmail/sendEmailScheNoti",
				    method: "POST",
				    contentType: "application/json",
				    data: JSON.stringify({"timeStamp": response[0]})
				  }).done(function(response){
				});
				$('#card').show();
				$('#dateChoice p').text(weekDay);
				$('#timeChoice p').text(time);
			}
			else{
				var allTimeStamp = [];
				var sortedTimeStamp = [];
				var sortedDay = [];
				var sortedTime = [];
				var sortedResponse = [];
				response.forEach(function(val){
					var testDateUtc = moment.unix(val.scheTimeStamp/1000);
					var localDate = moment(testDateUtc).local();
					allTimeStamp.push(val.scheTimeStamp);
					var hour = (localDate.hour()).toString();
					var minute = (localDate.minute()).toString();
					if(hour.length === 1){
						hour = '0' + hour;
					};
					if(minute.length === 1){
						minute = '0' + minute;
					};
					val.scheHour = hour;
					val.scheMinute = minute;
				});
				var allTimeStamp = allTimeStamp.sort();
				console.log(allTimeStamp);
				allTimeStamp.forEach(function(value, index){
					response.forEach(function(val){
						if(val.scheTimeStamp === value){
							sortedTimeStamp.push(val);
						};
					});
				});
				
				sortedTimeStamp.forEach(function(val){
					sortedTime.push(val.scheHour+" : "+val.scheMinute);
					sortedDay.push(val.scheDay);
					sortedResponse.push(val);
				});
				
				sortedTime = (sortedTime.filter((x, i, a) => a.indexOf(x) == i)).sort();
				sortedDay = sortedDay.filter((x, i, a) => a.indexOf(x) == i);
				console.log(sortedTime);
				console.log(sortedDay);
				addHeaderRow(sortedDay);
				addSlots(sortedResponse, sortedDay, sortedTime);
				$('button').show();
				submit(response);
			}
			

			
		});
	})
}

$('button').hide();
$('#card').hide();
main();
toggleCl();