$('body').on('click', 'button[type="submit"]', function(){
	var date = $('#datetimepicker').data('date');
	var hour = moment(date).hour();
	var minute = moment(date).minute();
	var timeStamp = moment(date).valueOf();
	var day = moment(timeStamp).format('dddd');
	console.log(day);
	console.log(hour.toString() + ' : ' + minute.toString());

	var scheHour = hour;
	var scheMinute = minute;
	var scheFull = date;
	var scheDay = day;
	var scheTimeStamp = timeStamp;
	var allSlots = $('input[name="number"]').val();
	$.ajax({
		url: "/api/sche",
		method: "POST",
		contentType: "application/json",
		data: JSON.stringify({
			'scheHour': scheHour,
			'scheMinute': scheMinute, 
			'scheFull': scheFull, 
			'scheDay': scheDay,
			'scheTimeStamp' : scheTimeStamp,
			'allSlots': allSlots
		})
	}).done(function(response){
		console.log(response);
	});
});

$('body').on('click', 'button#reset', function(){
	if(confirm("Are you sure to reset this user")){
		var username = $('input[name="reset"]').val();
		$.ajax({
			url: "/api/sche/resetSche",
			method: "PUT",
			contentType: "application/json",
			data: JSON.stringify({
				'username': username
			})
		}).done(function(response){
			console.log(response);
			$.ajax({
				url: "/api/user/resetUserSche",
				method: "PUT",
				contentType: "application/json",
				data: JSON.stringify({
					'username': username
				})
			}).done(function(res){
				console.log(res);
			});
		});
	}
});

$('body').on('click', 'button#resetAll', function(){
	if(confirm("Are you sure to reset all users")){
		$.ajax({
			url: "/api/sche/resetAllSche",
			method: "PUT"
			
		}).done(function(response){
			console.log(response);
			$.ajax({
				url: "/api/user/resetAllUserSche",
				method: "PUT"
				
			}).done(function(res){
				console.log(res);
			});
		});
	};
})

$('#datetimepicker').datetimepicker({
	format: 'YYYY-MM-DD HH:mm:ss',
});
