//Get data from multiselect

function dataMulti(idType, idTypeChecked, multiVal) {         
    var eachMulti = [];
    $(idTypeChecked).each(function() {
    	var value = $(this).siblings('label').text();
    	if (value === 'Other'){
    		value = $(this).siblings('.otherChoice').val();
    	};
    	eachMulti.push(value);
    	
    });
    multiVal.push({
		key: idType,
		value: eachMulti
	});
};

function dataText(textVal){
	$(".each-select > input").each(function(){
		textVal.push({
			key: $(this).attr('id'),
			value: $(this).val()
		});
	});
};

function dataSingle(singleVal){
	$(".select-wrapper").each(function(){
		singleVal.push({
			key: $(this).children('select').attr('id'),
			value: $(this).find('select option:selected').text()
		})
	});
};

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#imgId').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#inputId").change(function(){
    readURL(this);
});

// var listImage = [];
// $('#upload-input').on('change', function(){
// 	var files = $('#upload-input').get(0).files;
// 	for (var i = 0; i < files.length; i++) {
// 		listImage.push(files[i]);
// 	};
// });
// $('body').on('click', 'img.thumb', function(){
// 	$(this).closest('span').remove();
// 	console.log($(this).prop('title'));
// 	listImage.forEach(function(val, index){
// 		console.log(val.name);
// 		if(val.name === $(this).prop('title')){
// 			if(index != -1) {
// 				listImage.splice(index, 1);

// 			};
// 			console.log(listImage);
// 		};
// 	})
	
// });

var saveImage = function(username){
	  var files = $('#upload-input').get(0).files;
	  
	  
	  if (files.length > 0){
	    var formData = new FormData();
	    // loop through all the selected files
	    for (var i = 0; i < files.length; i++) {
	      var file = files[i];
	      formData.append('uploads[]', file, file.name);
	      // add the files to formData object for the data payload
	    };
	    
	 //    $('img.thumb').on('click', function(){
		// 	$(this).closest('span').remove();
		// 	formData.remove();
		// });
	    $.ajax({
		  url: '/api/photo',
		  type: 'POST',
		  data: formData,
		  processData: false,
		  contentType: false
		}).done(function(data){
			$.ajax({
			    url: "/api/user/photo",//POST data login to server for authentication
				method: "PUT",
				contentType : "application/json",
				data: JSON.stringify({"username": username})
			});
		})
	  }
};

$("#mainForm").on('click', 'a[type="submit"]', function(){
	var multiVal = [];
	var textVal = [];
	var singleVal = [];
	var allEmpty = [];
	var sendList = {};
	dataText(textVal);
	dataMulti('studyYear','#studyYear :checked', multiVal);
	dataMulti('wantedJob','#wantedJob :checked', multiVal);
	dataMulti('contactMethod','#contactMethod :checked', multiVal);
	dataSingle(singleVal);
	validateInput([textVal, singleVal, multiVal], allEmpty, sendList, function(sendList){
		$.ajax({
			url: "/api/user",
			method: "POST",
			contentType: "application/json",
			data: JSON.stringify(sendList)
		}).done(function(response){
			var username = $('input#username').val();
			var password = $('input#password').val();
			var fullName = $('input#fullName').val();
			var email = $('input#email').val();
			$.ajax({
			    url: "/api/auth/local",//POST data login to server for authentication
				method: "POST",
				contentType : "application/json",
				data: JSON.stringify({"username": username, "password": password})
				}).done(function(response) {
				saveImage(username);
			});
			$.ajax({
			    url: "/api/sendEmail/sendEmailSignup",
			    method: "POST",
			    contentType: "application/json",
			    data: JSON.stringify({"fullName": fullName, 'email': email, 'username': username})
			  }).done(function(response){
			  	alert("Please login via link in your email");
			    location.reload(true);
			});
		});
	});
});


