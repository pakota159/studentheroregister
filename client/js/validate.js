//====Show/Hide message
$(document).ready(function() {
	$("#mainForm").on('focus', '.each-select input', function(){
		
		$(this).closest('.each-select').children('.messageContainer').children('p').css('display', 'none');

	});
});

//====Validate Inut field

var validateInput = function(dataInput, allEmpty, sendList, cb){
	$(document).ready(function() {
		var emptyArr = [];
		dataInput.forEach(function(value){
			value.forEach(function(element){
				if(element.value === "Choose" || element.value === "" || element.value.length === 0){
					emptyArr.push(element.key);
					$('#'+element.key+'').closest('.each-select').find('.messageContainer').children('p').css('display', 'inherit');
				}
				else{
					var sendKey = element.key;
					sendList[sendKey] = element.value;
					$('#'+element.key+'').closest('.each-select').find('.messageContainer').children('p').css('display', 'none');
				};
				if(typeof(element.value) == 'object'){
					(element.value).forEach(function(val){
						if(val == ""){
							emptyArr.push(element.key);
							$('#'+element.key+'').closest('.each-select').find('.messageContainer').children('p').css('display', 'inherit');
						}
					});
				};
			});
		});
		if(emptyArr.length != 0){
			emptyArr.forEach(function(value){
				allEmpty.push(value);
			});
			var distancesArr = [];
			allEmpty.forEach(function(value){
				var distances = $('#'+value+'').closest('.each-select').offset().top;
				distancesArr.push(distances);
			});
			Array.prototype.min = function() {
	  			return Math.min.apply(null, this);
	  		};
			var highestEle = (allEmpty[distancesArr.indexOf(distancesArr.min())]);
			
			$('html, body').animate({
		        scrollTop: $('#'+highestEle+'').closest('.each-select').offset().top
		    }, 500);
		}else if(emptyArr.length == 0){
			cb(sendList);
		}
	});
};