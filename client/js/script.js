var initialEle = function(){
	$(document).ready(function() {
        $(function () {
		  // Grab the template script
		  var theTemplateScript = $("#input-template").html();

		  // Compile the template
		  var theTemplate = Handlebars.compile(theTemplateScript);

		  // Define our data object
		  $.getJSON("js/data.json", function(data) {
		  		$('.foreign-content').html(theTemplate(data));
		  		$('select').material_select();
		  		$(".each-select").find('input').prop('required', true);
		  		$("#job9").closest('.jobChoice').append("<input type='text' class='otherChoice'>");
		  });
		});
    });

    $(document).ready(function() {
        $(function () {
		  // Grab the template script
		  var theTemplateScript = $("#address-template").html();

		  // Compile the template
		  var theTemplate = Handlebars.compile(theTemplateScript);

		  // Define our data object
		  $.getJSON("js/data.json", function(data) {
		  		$('.local-content').html(theTemplate(data));
		  		$('select').material_select();
		  		$(".each-select").find('input').prop('required', true);
		  		$("#contact6").closest('.checkbox-edit').append("<input type='text' class='otherChoice'>");
		  });
		});
    });
};
// /append("<input type='text' id='otherChoice'>")
// initialEle(function(){
//     	$("#job9").closest('.jobChoice').append("<input type='text' id='otherChoice'>");
// });
initialEle();

$(".container").on("change", "#job9", function(){
	
	// if($('#otherChoice').focus()){
	// 	$("#job9").prop("checked");
	// };
});
	
$("body").on("click", ".container", function(){
	if($("#job9").prop("checked") === false){
		if($('.jobChoice .otherChoice').is(":focus")){
			$("#job9").prop("checked", true);
		}
	}else{
		if($('.jobChoice .otherChoice').is(":focus") === false){
			$("#job9").prop("checked", false);
		}
	};
	if($('.jobChoice .otherChoice').val() !== ''){
		$("#job9").prop("checked", true);
	};
	$('#job9').click(function(){
		$('.jobChoice .otherChoice').focus();
	});
});

$("body").on("click", ".container", function(){ 
	// if($("#contact6").prop("checked", false)){
	// 	if($('.checkbox-edit .otherChoice').is(":focus")){
	// 		$("#contact6").prop("checked", true);
	// 	}
	// }
	// else{
	// 	if($('.checkbox-edit .otherChoice').is(":focus") === false){
	// 		$("#contact6").prop("checked", false);
	// 	}
	// };
	if($('.checkbox-edit .otherChoice').val() !== ''){
		$("#contact6").prop("checked", true);
	};
	$('#contact6').click(function(){
		$('.checkbox-edit .otherChoice').focus();
	});
});

function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);
      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
}
document.getElementById('upload-input').addEventListener('change', handleFileSelect, false);

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    localStorage.setItem("scroll", scroll);
});

var scroll = localStorage.getItem("scroll");
console.log(scroll);
setTimeout(function(){
	$(window).scrollTop(scroll);
}, 1000);


