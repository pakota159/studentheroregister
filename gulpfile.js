var gulp = require('gulp');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var nodemon = require('gulp-nodemon');

gulp.task('sassSignup', function () {
  return gulp.src('client/scss/*.scss')
  .pipe(sass({outputStyle: 'compressed', sourceComments: 'map'}, {errLogToConsole: true}))
  .pipe(prefix("last 2 versions", "> 1%", "ie 8", "Android 2", "Firefox ESR"))
  .pipe(gulp.dest('client/css'))
  .pipe(reload({stream:true}));
});

gulp.task('sassSche', function () {
  return gulp.src('client/schedule/scss/*.scss')
  .pipe(sass({outputStyle: 'compressed', sourceComments: 'map'}, {errLogToConsole: true}))
  .pipe(prefix("last 2 versions", "> 1%", "ie 8", "Android 2", "Firefox ESR"))
  .pipe(gulp.dest('client/schedule/css'))
  .pipe(reload({stream:true}));
});

gulp.task('sassAdmin', function () {
  return gulp.src('client/admin/scss/*.scss')
  .pipe(sass({outputStyle: 'compressed', sourceComments: 'map'}, {errLogToConsole: true}))
  .pipe(prefix("last 2 versions", "> 1%", "ie 8", "Android 2", "Firefox ESR"))
  .pipe(gulp.dest('client/admin/css'))
  .pipe(reload({stream:true}));
});

gulp.task('sassLogin', function () {
  return gulp.src('client/login/scss/*.scss')
  .pipe(sass({outputStyle: 'compressed', sourceComments: 'map'}, {errLogToConsole: true}))
  .pipe(prefix("last 2 versions", "> 1%", "ie 8", "Android 2", "Firefox ESR"))
  .pipe(gulp.dest('client/login/css'))
  .pipe(reload({stream:true}));
});

gulp.task('browser-sync', ['nodemon'], function() {
  browserSync.init(null, {
    proxy: "http://localhost:7000",
    port: 3000,
  });
});

gulp.task('default', ['sassSignup', 'sassSche','sassAdmin', 'sassLogin', 'browser-sync'], function () {
  gulp.watch("client/scss/*.scss", ['sassSignup']);
  gulp.watch("client/schedule/scss/*.scss", ['sassSche']);
  gulp.watch("client/admin/scss/*.scss", ['sassAdmin']);
  gulp.watch("client/login/scss/*.scss", ['sassLogin']);
  gulp.watch(["**/*.js", "**/*.html", "**/*.scss"], reload);
});

gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({script: 'server.js'}).on('start', function () {
    if (!called) {
      called = true;
      cb();
    }
  });
});