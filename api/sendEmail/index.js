var express = require('express');
var controller = require('./mail.controller');

var router = express.Router();

router.post('/sendEmailSignup', controller.sendEmailSignup);
router.post('/sendEmailScheNoti', controller.sendEmailScheNoti);

module.exports = router;