var decode = require('../auth/decode');
var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');
var moment = require('moment');
var User = require('../user/user.model');
moment().format();

module.exports = {
	sendEmailSignup: function(req, res){
		var transporter = nodemailer.createTransport({
			service: 'Gmail',
		    auth: {
		        user: 'senddemoemail@gmail.com',
		        pass: 'phamtuancuong'
		    }
		});
		transporter.use('compile', hbs({
			viewPath:'api/sendEmail/template',
			extname: '.hbs'
		}));

		var mailOption = {
			from: 'senddemoemail@gmail.com',
			to: req.body.email,
			subject: '[Student Life Care] Thông tin đăng nhập Student Hero',
			template: 'emailSignup',
			context:{
				fullName: req.body.fullName,
				username: req.body.username
			}
		};
		transporter.sendMail(mailOption, function(err, info){
			if(err) throw err;
			res.send(info.response);
		});
	},

	sendEmailScheNoti: function(req, res){
		var transporter = nodemailer.createTransport({
			service: 'Gmail',
		    auth: {
		        user: 'senddemoemail@gmail.com',
		        pass: 'phamtuancuong'
		    }
		});
		transporter.use('compile', hbs({
			viewPath:'api/sendEmail/template',
			extname: '.hbs'
		}));

		//===============Set time stamp
		var dateUtc = moment.unix(req.body.timeStamp);
		console.log(req.body.timeStamp);
		var day = moment(dateUtc).local();
		var hour = day.hour().toString();
		var minute = day.minute().toString();
		var weekDay = moment(req.body.timeStamp*1000).format("dddd, MMMM Do YYYY");
		console.log(weekDay);
		if(hour.length === 1){
				hour = '0' + hour;
		};
		if(minute.length === 1){
			minute = '0' + minute;
		};
		var time = hour+" : "+minute;

		//======================================================
		//=============Find User
		var email = '';
		var idUser = decode.decodeUser(req);
		User.findOne({'_id': idUser}, function(err, user){
			if(err) throw err;
			var email = user.email;
			console.log(email);
			var mailOption = {
				from: 'senddemoemail@gmail.com',
				to: email,
				subject: '[Student Life Care] Thông báo lịch phỏng vấn Student Hero',
				template: 'emailScheNoti',
				context:{
					scheTime: time,
					scheDate: weekDay
				}
			};
			transporter.sendMail(mailOption, function(err, info){
				if(err) throw err;
				console.log(time);
				res.send(info.response);
			});
		});
		
	}
}