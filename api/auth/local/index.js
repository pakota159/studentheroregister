//define POST method when login
'use strict';

var express = require('express');
var User = require('../../user/user.model');
var authService = require('../auth.service');//use auth.service.js

var router = express.Router();

router.post('/', function(req, res){
    User.findOne({
      username: req.body.username
    }).select('password username role').exec(function(err, user){
      if(err) throw(err);
      if(!user){
        res.json({
          success: false,
          message: 'User not found'
        });
      }else if(user){

        var validPass = user.comparePass(req.body.password);
         if(!validPass){
          res.json({
            success: false,
            message: "Wrong password"
          })
        }else{
          console.log(user);
          var token = authService.signToken(user._id, user.username);//use signToken from authService
          console.log(token);
          res.cookie('token', token);
          
          var referrer = "";
          if(user.role == "student"){
            referrer = "../schedule";
          };
          res.json({token: token, referrer: referrer});
        };
      };
    });
});

module.exports = router;
