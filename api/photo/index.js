var express = require('express');
var controller = require('./photo.controller');

var router = express.Router();

router.post('/', controller.upPhoto);
// router.get('/', controller.showImg);

module.exports = router;