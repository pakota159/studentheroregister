var util = require("util"); 
var fs = require("fs");
var formidable = require('formidable');
var path = require('path');

module.exports = {
	
	upPhoto: function(req, res){
		var form = new formidable.IncomingForm();
		console.log(req.body);
		form.parse(req, function(err, fields, files){
			res.writeHead(200, {'content-type': 'text/plain'});
        	res.write('received upload:\n\n');
        	res.end(util.inspect({fields: fields, files: files}));
		});
		form.on('error', function(err) {
	        console.error(err);
	    });
	    form.multiples = true;
	    form.uploadDir = 'client/localphoto/';

	    form.on('file', function(field, file) {
		    fs.rename(file.path, path.join(form.uploadDir, file.name), function(err){
		    	if (err) throw err;
		    	// console.log(path.join(form.uploadDir, file.name));
		    	// console.log(file.path);
		    	// fs.unlink(path.join(form.uploadDir, file.name), function(){
		    	// 	if(err) throw err;
		    	// })
		    });

			// photo.img.data = fs.readFileSync('client/localphoto/' + file.name);
			// photo.img.contentType = 'image/*';
			// photo.save(function(err){
			// 	if(err) throw err;
			// });
		});
		form.on('end', function() {
		    res.end('success');
		});
	}

	// showImg: function(req, res){
	// 	Photo.find({}, function(err, photo){
	// 		if(err) throw err;
	// 		var base64 = (photo[0].img.data.toString('base64'));
	// 		res.contentType(photo[0].img.contentType);
	// 		res.send(base64);
	// 	})
	// }

}