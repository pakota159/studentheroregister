var Sche = require('./sche.model');
var decode = require('../auth/decode');
var User = require('../user/user.model');
var async = require("async");
var moment = require('moment');
moment().format();

module.exports = {
	
	createSche: (function(req, res){
		Sche.find({'scheTimeStamp': req.body.scheTimeStamp}, function(err, sche){
			if(sche.length !== 0){
				res.json({message:"sche has already set - Please choose another schedule"});
			}else{
				var sche = new Sche();
				sche.scheHour = req.body.scheHour;
				sche.scheMinute = req.body.scheMinute;
				sche.scheFull = req.body.scheFull;
				sche.scheDay = req.body.scheDay;
				sche.scheTimeStamp = req.body.scheTimeStamp;
				sche.allSlots = req.body.allSlots;
				sche.remainSlots = req.body.allSlots;
				sche.save(function(err){
					if(err){
							return res.send(err);
					}else{
						res.json({message:"create schedule success"});
					}
				});
			};
		})
		
	}),

	getSche: (function(req, res){
		var idUser = decode.decodeUser(req);
		User.findOne({'_id': idUser}, function(err, user){
			console.log(user.timeStamp);
			if(user.timeStamp !== 0){
				var data = [];
				data.push(user.timeStamp);
				console.log('already has timestap');
				res.send(data);
			}else{
				Sche.find({}, function(err, sche){
					if(err) throw err;
					res.send(sche);
				})
			}
		})
	}),

	changeSlot: (function(req, res){
		var timeStamp = parseInt(req.body.timeStamp);
		User.find({'timeStamp': timeStamp}, function(err, user){
			if(err) throw err;
			console.log('user found');
			console.log(user);
			var takenSlots = user.length;
			console.log(takenSlots);
			Sche.findOne({'scheTimeStamp': (timeStamp*1000)}, function(err, sche){
				if(err) throw err;
				if(sche.remainSlots > 0){
					sche.remainSlots = sche.allSlots - takenSlots;
				};
				sche.save();
				console.log('sche saved');
				console.log(sche);
				res.send(sche);
			})
		})
		
	}),

	resetSche: (function(req, res){
		User.findOne({'username': req.body.username}, function(err, user){
			if(err) throw err;
			if(user.timeStamp === 0){
				res.json({message: 'timeStamp is already 0'})
			}else{
				Sche.findOne({'scheTimeStamp': user.timeStamp*1000}, function(err, sche){
					if(sche.remainSlots < sche.allSlots){
						sche.remainSlots += 1;
					};
					sche.save();
					res.json({message:'reset sche successful'});
				})
			}			
		})
	}),

	resetAllSche: (function(req, res){
		Sche.find({}, function(err, sche){
			async.forEachOf(sche, function(value, key, callback){
				value.remainSlots = value.allSlots;
				value.save();
				callback();
			}, function(err){
				if(err) throw err;
				
				res.json({message:'reset all sche successful'});
			});
		});
	})

}