var express = require('express');
var controller = require('./sche.controller');

var router = express.Router();

router.post('/', controller.createSche);
router.get('/', controller.getSche);
router.put('/', controller.changeSlot);
router.put('/resetSche', controller.resetSche);
router.put('/resetAllSche', controller.resetAllSche);

module.exports = router;