//define variables
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var shortId = require('shortid');

// create user Schema
var ScheSchema = new Schema({
	_id: {type: String, unique: true, 'default': shortId.generate},
	scheFull: String,
	scheHour: Number,
	scheMinute: Number,
	scheDay: String,
	scheTimeStamp: Number,
	allSlots: Number,
	remainSlots: Number
});

module.exports = mongoose.model('Sche', ScheSchema);