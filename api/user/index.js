var express = require('express');
var controller = require('./user.controller');

var router = express.Router();

router.post('/', controller.createUser);
router.get('/', controller.getAll);
router.put('/', controller.userSlot);
router.put('/photo', controller.createImg);
router.put('/resetUserSche', controller.resetUserSche);
router.put('/resetAllUserSche', controller.resetAllUserSche);

module.exports = router;