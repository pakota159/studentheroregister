//define variables
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var shortId = require('shortid');
var bcrypt = require('bcrypt-nodejs');

// create user Schema
var UserSchema = new Schema({
	_id: {type: String, unique: true, 'default': shortId.generate},
	fullName: {type: String},
	gender: {type: String},
	dateOfBirth: {type: Date, required: false},
	vnCity: {type: String, required: false},
	foreignCountry: {type: String, required: false},
	foreignCity: {type: String, required: false},
	timeLiveInCity: {type: String, required: false},
	foreignUni: {type: String, required: false},
	foreignCampus: {type: String, required: false},
	major: {type: String, required: false},
	studyYear: {type: Array, required: false},

	foreignStreet: {type: String, required: false},
	foreignCityLive: {type: String, required: false},
	foreignState: {type: String, required: false},
	postcode: {type: Number, required: false},
	foreignPhone: {type: Number, required: false},
	email: {type: String, required: false},
	facebook: {type: String, required: false},
	bankInfo: {type: String},
	currentJob: {type: String, required: false},
	wantedJob: {type: Array, required: false},
	workTime: {type: String, required: false},
	contactTime: {type: Array, required: false},
	contactMethod: {type: String, required: false},
	vehicle: {type: String, required: false},
	comment: {type: String, required: false},
	listImg: [{
		img: { data: Buffer, contentType: String }
	}],
	username: {type: String, index: {unique: true}},
	password: {type: String, select: false},
	role: {type: String},

	timeStamp: Number
});

//encrypt password
UserSchema.pre('save', function(next){
	var user = this;
	//only encrypt if new password or modified
	if (!user.isModified('password')) return next();

	bcrypt.hash(user.password, null, null, function(err, hash){
		if (err) return next(err);
		user.password = hash;
		next();
	});
});

UserSchema.methods = {comparePass : (function(password){
  	var user = this;
  	return bcrypt.compareSync(password, user.password);
  })
};

module.exports = mongoose.model('User', UserSchema);