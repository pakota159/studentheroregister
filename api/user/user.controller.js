var User = require('./user.model');
var randomstring = require("randomstring");
var decode = require('../auth/decode');
var async = require("async");
var fs = require("fs");

module.exports = {
	
	createUser: (function(req, res){
		var user = new User();
		user.fullName = req.body.fullName;
		user.gender = req.body.gender;
		user.dateOfBirth = req.body.dateOfBirth;
		user.vnCity = req.body.vnCity;
		user.foreignCountry = req.body.foreignCountry;
		user.foreignCity = req.body.foreignCity;
		user.timeLiveInCity = req.body.timeLiveInCity;
		user.foreignUni = req.body.foreignUni;
		user.foreignCampus = req.body.foreignCampus;
		user.major = req.body.major;
		user.studyYear = req.body.studyYear;

		user.foreignStreet = req.body.foreignStreet;
		user.foreignCityLive = req.body.foreignCityLive;
		user.foreignState = req.body.foreignState;
		user.postcode = req.body.postcode;
		user.foreignPhone = req.body.foreignPhone;
		user.email = req.body.email;
		user.facebook = req.body.facebook;
		user.bankInfo = req.body.bankInfo;
		user.currentJob = req.body.currentJob;
		user.wantedJob = req.body.wantedJob;
		user.workTime = req.body.workTime;
		user.contactTime = req.body.contactTime;
		user.contactMethod = req.body.contactMethod;
		user.vehicle = req.body.vehicle;
		user.comment = req.body.comment;
		user.listImg = [];

		user.timeStamp = 0;

		user.username = req.body.username;
		user.password = req.body.password;
		user.role = 'student';
		
		user.save(function(err){
			if(err){
					return res.send(err);
			}else{
				res.json({message:"create user success"});
			}
		});
	}),

	getAll: (function(req, res){
		User.find({},function(err, user){
			if(err) {
				throw(err);
			}
			else{
				var sendArr = [];
				var subArr = [];
				var check = user[0].listImg[0].img.data.toString('base64')
				user.forEach(function(usr){
					subArr.push(usr);
					(usr.listImg).forEach(function(obj){
						var base64 = obj.img.data.toString('base64');
						subArr.push(base64);
					});
					sendArr.push(subArr);
					subArr = [];;
				});
				res.send(sendArr);
			}
		});
	}),

	userSlot: (function(req, res){
		var idUser = decode.decodeUser(req);
		var timeStamp = req.body.timeStamp;
		User.findOne({'_id': idUser}, function(err, user){
			if(err) throw err;
			user.timeStamp = timeStamp;
			user.save();
			console.log('user saved timestamp');
			console.log(user);
			res.send(user);
		})
	}),

	resetUserSche : (function(req, res){
		User.findOne({'username': req.body.username}, function(err, user){
			if(err) throw err;
			if(user.timeStamp === 0){
				res.json({message : 'user timeStamp is already 0'});
			}else{
				user.timeStamp = 0;
				user.save();
				res.json({message : 'reset user successful'});
			}
		});
	}),

	resetAllUserSche : (function(req, res){
		User.find({}, function(err, user){
			async.forEachOf(user, function(value, key, callback){
				value.timeStamp = 0;
				value.save();
				callback();
			}, function(err){
				if(err) throw err;
				res.json({message:'reset all user timestamp successful'});
			});
		});
	}),

	createImg: (function(req, res){
		User.findOne({'username': req.body.username}, function(err, user){
			if(err) throw err;
			const folder = 'client/localphoto';
			var files = fs.readdirSync(folder);
			function updateImg(cb){
				files.forEach(function(val){
					var data = fs.readFileSync('client/localphoto/' + val);
					var contentType = 'image/*';
					User.update({'username': req.body.username}, {
						$push: {"listImg": {'img.data': data, 'img.contentType': contentType}}
					},
			    	{safe: true, upsert: true, multi: true},function(err){
						if(err) throw(err);
						fs.unlink(path.join('client/localphoto/' + val), function(){
				    		if(err) throw err;
				    	})
					});
				});
				cb();
			};			
			updateImg(function(){
				user.save();
				res.send(user);
			});
		});
	})
}