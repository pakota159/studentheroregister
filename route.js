var path = require('path');

module.exports = function(app) {
  app.use('/api/user', require('./api/user'));
  app.use('/api/auth', require('./api/auth'));
  app.use('/api/sche', require('./api/sche'));
  app.use('/api/sendEmail', require('./api/sendEmail'));
  app.use('/api/photo', require('./api/photo'));
}